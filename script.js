// console.log("Hello World");

// [SECTION] JSON Objects
// JSON stands for Javascript Object Notation
// "Parsing" and "Stringify"
// JSON is used for serializing different data type

/*

{
    "propertyA" : "valueA",
    "propertyB" : "valueB"
}

*/

// JSON Object

/*{
    "city" : "Quezon City",
    "province" :  "Metro Manila",
    "country" : "Philippines"
} */

// JSON Array

// arrayName = cities

/* "cities" : [
    {
        "city" : "Quezon City", 
        "province" : "Metro Manila",
        "country" : "Philippines"
    },
    {
        "city" : "Manila City",
        "province" : "Metro Manila",
        "country" : "Philippines"
    },
    {
        "city" : "Makati City",
        "province" : "Metro Manila",
        "country" : "Philippines"
    }
] */

// [SECTION] JSON Methods
// the JSON object contains methods for parsing and converting data into stringified JSON

let batchesArr = [
    {
        batchName: "Batch 217",
        schedule: "Full-Time",
    },
    {
        batchName: "Batch 218",
        schedule: "Part-time"
    }
]

// stringify --> converting method for JS objects into a string
console.log("Result from stringify method:");
console.log(JSON.stringify(batchesArr));

// converting directly to stringify
let data = JSON.stringify({
    name: "John",
    age: 31,
    address: {
        city: "Manila",
        country: "Philippines"
    }
});
console.log(data);

// [SECTION] using stringify method with variables 

let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age");
let address = {
    city: prompt("Which city do you live in?"),
    country: prompt("Which country does your city address belongs to?"),
}

// otherData will hold the above input
let otherData = JSON.stringify({
    firstName: firstName,
    lastName: lastName,
    age: age,
    address: address
});

console.log(otherData);

// [SECTION] Converting Stringified JSON into JS Objects

// Parse Method

let batchesJSON = `[
    {
        "batchName" : "Batch 217",
        "schedule" : "Full-Time"
    },

    {
        "batchName" : "Batch218",
        "schedule" : "Part-Time"
    }
]`

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));

let stingifiedObject = `{
    "name" : "John",
    "age" : "31",
    "address" : {
        "city" : "Manila",
        "country" : "Philippines"
    }
}`

console.log(JSON.parse(stingifiedObject));